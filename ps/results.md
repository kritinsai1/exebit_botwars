#Botwars Results

##Teams

Team No | Team Members
--- | ---
0   | Random bot
4   | Vignesh Manoharan
6   | Srivishnu, Dharaniraj
8   | Venketep prasad, Shashikant reddy
9   | Vanya BK, Manognya P Koolath
10  | Abdun Nihaal A N, Akash S

A graph with 30 nodes was randomly generated and used for the evaluation, it had about 200 edges, the pickled file can be found [here](../graphs/graph_2018-04-15-17-51-37)

You can find the evaluation script [here](../tournament.py)

##The scores

(One point for a win in a match)

Team |    Score
--- | ---
4|       8
9 |      8
8  |     6
10  |    5
6    |   2
0     |  0

As teams 4 and 9 had the same score, their performance against each other was used to break the tie.

Team 4 vs Team 9 (same graph):

Team |   Score
--- | ---
4    |   2
9    |   0

##Ranking

Therefore the final ranking is:

Team| Team members| Rank
---|---|---
4|Vignesh Manoharan| 1
9|Vanya BK, Manognya P Koolath| 2
8 |Venketep prasad, Shashikant reddy| 3
10 | Abdun Nihaal A N, Akash S|4
6   | Srivishnu, Dharaniraj| 5
0|   Random bot|  6