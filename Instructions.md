# Planets and Wormholes

You are the commander of a spaceship. Your mission is to conquer as many planets as possible in the galaxy G42. Some  planets are connected by wormholes. You have a rival spaceship. A planet is conquered when it is disconnected from all the other planets. This can be done by converting the wormholes connecting the planets to blackholes. In your turn you can convert 1 wormhole to a blackhole. The player who converts the last connecting wormhole of a planet conquers the planet. Once a planet is conquered, you gain energy to convert 1 more wormhole which you need to compulsorily use to convert another wormhole (if any exist). The game ends when all wormholes are converted. The player with the highest number of conquered planets wins. If the number of planets is the same, it is a draw.

**Essentially**, you have a graph with nodes and edges, you need to choose an edge to remove. If the edge is the last connecting edge for one of the two nodes, you capture that (or both) node(s) and you need to remove another edge compulsorily. Play alternates between you and the opponent. The game ends when all edges are removed. The player with the highest number of captured wins

## To do
You need to implement a python function `bot` in a file `team_[your_team_no].py` with the following signature:

```python
def bot(graph):
    ...
    return move
```

`move` is a tuple `(n0, n1)` where `n0, n1` are the nodes (in `0, 1, 2...`) connected by the edge.
`graph` is an object with the following attributes and functions:
```python
nedges # the number of edges
nnodes # the number of nodes
# Nodes of the graph are numbered from 0 to nnodes-1
edges # A dict of the following form:
# {
#     node_num: set([nodes to which it is connected]),
#     ...
# }
has_edge(self, edge) # returns True if the graph contains the edge
remove_edge(self, edge) # remove edge from the graph
all_edges(self) # returns a list of all edges in the graph
copy(self) # makes copy of the graph
```
As an example a bot which makes random moves is given in the file `game_env.py`
## Evaluation

Your bot will play with the bots of all other teams. With each team two matches will be played once with you as the first player, and then as the second player. You get a point if you win a match. The team with the highest score wins.