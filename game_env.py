import random
import copy
import sys
import argparse
# Nodes and Edges


class Graph:
    ### State vars are edges, nedges ###
    def __init__(self, nnodes, edges):
        self.nnodes = nnodes
        self.edges = copy.deepcopy(edges)
        self.nedges = self.get_num_edges(edges)

    def get_num_edges(self, edges):
        ne = 0
        for n1 in self.edges:
            for n2 in self.edges[n1]:
                ne += 1
        assert ne % 2 == 0
        return ne/2

    # edge represented as (n1, n2)
    def has_edge(self, edge):
        n1 = edge[0]
        n2 = edge[1]
        assert n1 < self.nnodes and n2 < self.nnodes
        if edge[1] in self.edges[edge[0]]:
            return True
        else:
            return False

    def remove_edge(self, edge):
        assert self.has_edge(edge)
        self.edges[edge[0]].remove(edge[1])
        self.edges[edge[1]].remove(edge[0])
        self.nedges -= 1

    def copy(self):
        cgraph = Graph(self.nnodes, self.edges)
        return cgraph

    # returns all edges including duplicates like (n1, n2), (n2, n1)
    def all_edges(self):
        edgs = []
        for n1 in self.edges:
            for n2 in self.edges[n1]:
                edgs.append((n1, n2))
        assert 2 * self.nedges == len(edgs)
        return edgs


class GameState:
    cp = 0  # current player, can be 0 or 1

    def __init__(self, graph, current_player):
        self.graph = graph
        assert self.graph.nedges != 0, "Empty graph!"
        self.cp = current_player
        self.owned_nodes = [set() for i in range(2)]

    # move is of the form (n1, n2), where n1, n2 are node numbers
    def make_move(self, move):
        n1 = move[0]
        n2 = move[1]
        if self.graph.has_edge(move):
            d1 = len(self.graph.edges[n1])
            d2 = len(self.graph.edges[n2])
            self.graph.remove_edge(move)
            if d1 == 1 or d2 == 1:
                self.cp = self.cp
                if d1 == 1:
                    if args.debug:
                        print "bot {} captured node {}".format(self.cp, n1)
                    self.owned_nodes[self.cp].add(n1)
                if d2 == 1:
                    if args.debug:
                        print "bot {} captured node {}".format(self.cp, n2)
                    self.owned_nodes[self.cp].add(n2)
                if args.debug and False:
                    print "owned_nodes"
                    print self.owned_nodes
            else:
                self.cp = 1 - self.cp

            if self.graph.nedges == 0:
                # Game over.
                return -1
            else:
                return 1

        else:
            assert False, "Invalid move"
    
    def copy(self):
        cgraph = self.graph.copy()
        cgame_state = GameState(cgraph, self.cp)
        cgame_state.owned_nodes = copy.deepcopy(self.owned_nodes)
        return cgame_state


def graph_init(nnodes, p_edge):
    nodes = range(nnodes)
    edges = {}

    for node in nodes:
        edges[node] = set()

    for n1 in nodes:
        for n2 in nodes:
            if n1 != n2:
                p = random.random()
                if p < p_edge:
                    edges[n1].add(n2)
                    edges[n2].add(n1)

    return Graph(nnodes, edges)


def random_bot(gstate):
    graph = gstate.graph
    valid_moves = graph.all_edges()
    move = random.choice(valid_moves)
    return move

# given the GameState
# DO NOT expose this in the ps code, gstate can be accessed by the bot func


def game_controller(gstate, bot0, bot1):
    bots = [bot0, bot1]
    won = -1
    nmoves = gstate.graph.nedges
    while True:
        cgame_state = gstate.copy()
        cp = gstate.cp
        move = bots[cp](cgame_state)
        if args.debug:
            print "Move by bot {}: {}".format(cp, move)
        try:
            ret = gstate.make_move(move)
            nmoves -= 1
        except AssertionError:
            print "Error in bot {}".format(gstate.cp)
            won = 1 - gstate.cp
            ret = -1
        except:
            print "Error in make_move, exiting"
            sys.exit(1)

        if args.debug:
            print "current graph:"
            print gstate.graph.edges

        if ret == -1:
            break
    assert nmoves == 0, "Num edges not zero"
    if won == -1:
        b0_nnodes = len(gstate.owned_nodes[0])
        b1_nnodes = len(gstate.owned_nodes[1])

        print b0_nnodes, b1_nnodes
        if b0_nnodes > b1_nnodes:
            won = 0
        elif b0_nnodes == b1_nnodes:
            won = 2
        else:
            won = 1
        if args.debug:
            if won in [0, 1]:
                print "bot {} won by {} nodes".format(won, abs(b0_nnodes-b1_nnodes))
            else:
                print "draw"


    return won


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", dest="debug", action='store_true', default=False)
    parser.add_argument("--p_edge", help="density of edges", default=.1, type=float)
    parser.add_argument("--nnodes", help="number of nodes", default=4, type=int)

    args = parser.parse_args()

    game_graph = graph_init(args.nnodes, args.p_edge)
    if args.debug:
        print "Initial graph"
        print game_graph.edges

    game_state = GameState(game_graph, 0)
    bot0 = random_bot
    bot1 = random_bot

    print game_controller(game_state, bot0, bot1)

# print nodes
# print edges
