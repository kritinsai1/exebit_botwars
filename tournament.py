import importlib
import argparse
import game_env
import time
import pickle

parser = argparse.ArgumentParser()
parser.add_argument("--debug", dest="debug", action='store_true', default=False)
parser.add_argument("--tdebug", dest="tdebug", help="Debugging flag for current file", action='store_true', default=False)
parser.add_argument("--p_edge", help="density of edges", default=.1, type=float)
parser.add_argument("--nnodes", help="number of nodes", default=4, type=int)
parser.add_argument("--use_graph", help="graph file path to use as initial graph", default=None)

args = parser.parse_args()
game_env.args = args

### globals ###
team_nums = [4, 9] # each team file name would be team_<team_no>.py and would have a bot function
graph_file_name = "graphs/graph_{}".format(time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime()))

teams = {
    i: {
    "score": 0,
    "bot": None
    } for i in team_nums
}

def get_bot(file_name):
    module_name = "teams."+file_name
    imported_module = importlib.import_module(module_name)
    bot = getattr(imported_module, "bot")
    return bot

for i in team_nums:
    team_file = "team_{}".format(i)
    teams[i]["bot"] = get_bot(team_file)

if args.use_graph == None:
    # Make graph
    game_graph = game_env.graph_init(args.nnodes, args.p_edge)
    with open(graph_file_name, "w") as gf:
        pickle.dump(game_graph, gf)
        if args.debug:
            print "Saved graph to {}".format(graph_file_name)
else:
    with open(args.use_graph) as gf:
        game_graph = pickle.load(gf)
        if args.debug:
            print "Loaded graph from {}".format(args.use_graph)
if args.debug:
    print "Initial graph"
    print game_graph.edges

for t0 in team_nums:
    for t1 in team_nums:
        if t0 != t1:
            if args.tdebug:
                print "Play btw {}, {}".format(t0, t1)
            game_state = game_env.GameState(game_graph.copy(), 0)
            bot0 = teams[t0]["bot"]
            bot1 = teams[t1]["bot"]

            won = game_env.game_controller(game_state, bot0, bot1)
            if won in [0,1]:
                winner = [t0, t1][won]
                teams[winner]["score"] += 1

teams_by_score = sorted(team_nums, key=lambda x: teams[x]["score"], reverse=True)
print "Team\tScore"
for team in teams_by_score:
    print "{}\t{}".format(team, teams[team]["score"])